<?php

/**
* @file
* Contains forms and form handler functions that make up the 
* messaging_set_user_to_default_settings administration section.
*/


/**
* Use Drupal's Form API (FAPI) to wire up buttons to kick off batch operations.
*/
function messaging_set_user_to_default_settings_form(&$form_state) {

  $form['user_batches'] = array(
   '#type' => 'fieldset',
   '#title' => 'Reset messaging settings',
   '#description' => 'Run batch operations for users to reset to default all individual settings on messaging and notifications by a user.',
   '#collapsible' => TRUE,
   '#collapsed' => FALSE,
  );
  $form['user_batches']['batch_happy_message'] = array(
    '#type' => 'submit',
    '#value' => t('Make me happy'),
  );
 
  return $form;
}

/**
* Submit handler for messaging_set_user_to_default_settings_form();
*/
function messaging_set_user_to_default_settings_form_submit($form, &$form_state) {

  require_once(drupal_get_path('module', 'messaging_set_user_to_default_settings') .'/includes/messaging_set_user_to_default_settings.users.inc');
  messaging_set_user_to_default_settings_execute();
}