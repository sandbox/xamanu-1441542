<?php

/**
* Set up batch for first and last name loading.
*
* This is where Drupal's Batch API comes into play.
* It's as simple as defining $batch, and then calling batch_set($batch)
*/
function messaging_set_user_to_default_settings_execute() {

  // Update the user profiles to add values to newly added profile fields
  $batch = array(
    'title' => t('Updating User Accounts'), // Title to display while running.
    'operations' => array(), // Operations to complete, in order. Defined below.
    'finished' => '_messaging_set_user_to_default_settings_batch_finished', // Last function to call.
    'init_message' => t('Initializing...'),
    'progress_message' => t('Updated @current out of @total.'),
    'error_message' => t('Updating of profiles encountered an error.'),
  );

  // Add as many operations as you need. They'll run in the order specified.
  // Parameters can be defined in the (currently) empty arrays and will need
  // to also be added following the $context parameters for the operation
  // functions below.
  $batch['operations'][] = array('_messaging_set_user_to_default_settings_batch', array());

  // Tip the first domino.
  batch_set($batch);
}

// Our first batch operation.  Note: $context contains Batch API data.
function _messaging_set_user_to_default_settings_batch(&$context) {

  $messaging_default_method = variable_get('messaging_default_method', '');
  $notifications_default_send_interval = variable_get('notifications_default_send_interval','');

  // This is not very robust, but you get the point.
  // The profile values are saved in {user.data} if we have a profile_firstName there, exclude.
  $count = db_fetch_object(db_query("SELECT COUNT(*) as count FROM {users} WHERE 1 AND uid != 0 AND (data LIKE '%%messaging_default%%' OR data LIKE '%%notifications_send_interval%%')"));
  $results = db_query("SELECT * FROM {users} WHERE 1 AND uid != 0 AND (data LIKE '%%messaging_default%%' OR data LIKE '%%notifications_send_interval%%')");

  while ($row = db_fetch_object($results)) {
    $account = user_load($row->uid);
    $edit = array(
      'messaging_default' => $messaging_default_method,
      'notifications_send_interval' => $notifications_default_send_interval,
    );

    // Save user.
    user_save($account, $edit);
    drupal_set_message('Updated fields for ' . $account->name . ' [' . $account->uid . ']');

  }

  // Until $context['finished'] returns true, this will continue to iterate,
  // updating 20 user profiles at a time.
//  if ($count->count > 0) {
//    $context['finished'] = 0;
//  }
//  else {
    $context['finished'] = 1;
//  }
}

/**
* The function called when we finish. Displays a success or error message,
* but could do anything.
*/
function _messaging_set_user_to_default_settings_batch_finished($success, $results, $operations) {

  if ($success) {
    $message = t("All users' settings on messaging and notifications have been reset");
  }
  else {
    $message = t("Finished with error.");
  }
  drupal_set_message($message);
}
?>